module Game {

  export class Position {

    public static w(): number {
      return App.width;
    }

    public static h(): number {
      return App.height;
    }

    public static right(w: number = 0): number {
      return Position.w() - w;
    }

    public static bottom(h: number = 0): number {
      return Position.h() - h;
    }

    public static center(w: number = 0): number {
      return (Position.w()-w)/2;
    }

    public static middle(h: number = 0): number {
      return (Position.h()-h)/2;
    }

    public static midpoint(w: number = 0, h: number = 0): PIXI.Point {
      return new PIXI.Point(Position.center(w), Position.middle(h));
    }

  }

}