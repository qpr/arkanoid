const enum Key {
  Left  = 37,
  Right = 39,
  A     = 65,
  D     = 68,
  Space = 32,
  Enter = 13,
}

module App {

  class Button {

    public isPressed: boolean;
    public isReleased: boolean;

    constructor(private codes: number[], private onPress?: () => void) {
      this.isPressed = false;
      this.isReleased = true;

      window.addEventListener('keydown', this.pressHandler.bind(this), false);
      window.addEventListener('keyup', this.releaseHandler.bind(this), false);
    }

    private pressHandler(e: KeyboardEvent): void {

      if (this.codes.indexOf(e.keyCode) > -1 && this.isReleased) {
        this.isPressed = true;
        this.isReleased = false;
        if (this.onPress != null) {
          this.onPress();
        }
        e.preventDefault();
      }
    }

    private releaseHandler(e: KeyboardEvent): void {
      if (this.codes.indexOf(e.keyCode) > -1 && this.isPressed) {
        this.isPressed = false;
        this.isReleased = true;
        e.preventDefault();
      }
    }

    public free(): void {
      window.removeEventListener('keydown', this.pressHandler);
      window.removeEventListener('keyup', this.releaseHandler);
    }

  }

  export class Input {

    private leftBtn: Button;
    private rightBtn: Button;
    private actionBtn: Button;

    private actionListeners: (() => void)[];

    constructor() {
      this.actionListeners = [];
      this.leftBtn  = new Button([ Key.A, Key.Left ]);
      this.rightBtn = new Button([ Key.D, Key.Right ]);
      this.actionBtn = new Button([ Key.Enter, Key.Space ], this.actionHandler.bind(this));
    }

    private actionHandler(): void {
      for (let listener of this.actionListeners) {
        listener();
      }
    }

    public onAction(listener: () => void): void {
      this.actionListeners.push(listener);
    }

    public isLeft(): boolean {
      return (this.leftBtn.isPressed && this.rightBtn.isReleased);
    }

    public isRight(): boolean {
      return (this.leftBtn.isReleased && this.rightBtn.isPressed);
    }

    public isBoth(): boolean {
      return (this.leftBtn.isPressed && this.rightBtn.isPressed);
    }

    public isNone(): boolean {
      return (this.leftBtn.isReleased && this.rightBtn.isReleased);
    }

    public free():void {
      this.leftBtn.free();
      this.rightBtn.free();
    }

  }

}