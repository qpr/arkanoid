/// <reference path='geometry.ts'/>

module Game {

  interface ISegment {
    a: PIXI.Point;
    b: PIXI.Point;
    key: string;
    listener?: () => void;
  }

  export class Collider {

    private segments: ISegment[];
    private platform: ISegment;
    private platformSize: number;

    constructor() {
      this.segments = [];
      this.platformSize = 100;
      let startX = Position.center(this.platformSize);
      this.platform = {
        a: new PIXI.Point(startX, 549),
        b: new PIXI.Point(startX + this.platformSize, 549),
        key: 'platform'
      };
      this.segments.push(this.platform);
    }

    public addPoly(poly: PIXI.Point[], key: string, circular: boolean, listener?: () => void) {
      let pa = (!circular) ? poly.shift() : poly[poly.length-1];
      for (let pb of poly) {
        this.addSegment(pa, pb, key, listener);
        pa = pb;
      }
    }

    public addSegment(pa: PIXI.Point, pb: PIXI.Point, key: string, listener?: () => void) {
      let segment: ISegment = {
        a: pa,
        b: pb,
        key: key
      };
      if (listener != null) segment.listener = listener;
      this.segments.push(segment);
    }

    public movePlatform(x: number) {
      this.platform.a.x = x;
      this.platform.b.x = x + this.platformSize;
    }

    public removeBricks() {
      this.segments = this.segments.filter((segment) => segment.key.indexOf('brick') == -1);
    }

    public removeSegments(key: String) {
      this.segments = this.segments.filter((segment) => segment.key != key);
    }

    public move(state: IBallState): IBallState {
      let distance = state.speed * App.ticker.deltaTime;
      let next = new PIXI.Point(
        state.point.x + state.direction.x * distance,
        state.point.y + state.direction.y * distance
      );
      for (let segment of this.segments) {
        let dist = Geometry.distanceToSegment(next, segment.a, segment.b);
        if (dist < Ball.radius*0.9) {
          let normal = new Vector(segment.b.y - segment.a.y, segment.b.x - segment.a.x).normalize();
          state.direction.reflect(normal);
          next = new PIXI.Point(
            state.point.x + state.direction.x * distance,
            state.point.y + state.direction.y * distance
          );
          if (segment.listener != null) segment.listener();
        }
      }
      state.point = next;
      return state;
    }

  }
}