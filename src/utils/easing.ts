module Game {

  export class Easing {

    private min: number;
    private max: number;
    public value: number;

    constructor (private inc: number, private dec: number, private magnitude: number) {
      this.min = -this.magnitude;
      this.max = this.magnitude;
      this.value = 0;
    }

    public increase() {
      if(this.value >= this.max) return;
      this.value += this.inc * App.ticker.deltaTime;
      if(this.value > this.max) this.value = this.max;
    }

    public decrease() {
      if(this.value <= this.min) return;
      this.value -= this.dec * App.ticker.deltaTime;
      if(this.value < this.min) this.value = this.min;
    }

    public fading() {
      if (this.value > this.dec) {
        this.decrease();
      } else if (this.value < -this.dec) {
        this.increase();
      } else if (this.value != 0) {
        this.value = 0;
      }
    }

    public reset() {
      this.value = 0;
    }

  }

}