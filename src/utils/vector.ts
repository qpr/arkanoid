module Game {

  export class Vector extends PIXI.Point {

    public length(): number {
      return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public normalize(): Vector {
      let l = this.length();
      this.x /= l;
      this.y /= l;
      return this;
    }

    public dot(v: Vector): number {
      return this.x * v.x + this.y * v.y;
    }

    public reflect(normal: Vector): Vector {
      let scalar = this.dot(normal);
      this.x -= 2 * scalar * normal.x;
      this.y -= 2 * scalar * normal.y;
      return this.normalize();
    }

  }

}