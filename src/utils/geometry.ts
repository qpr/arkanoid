module Game {

  export class Geometry {

    public static distanceToSegment(pt: PIXI.Point, p1: PIXI.Point, p2: PIXI.Point): number {
      let dx = p2.x - p1.x;
      let dy = p2.y - p1.y;
      if ((dx == 0) && (dy == 0)) {
        dx = pt.x - p1.x;
        dy = pt.y - p1.y;
        return Math.sqrt(dx * dx + dy * dy);
      }

      let t = ((pt.x - p1.x) * dx + (pt.y - p1.y) * dy) / (dx * dx + dy * dy);
      if (t < 0) {
        dx = pt.x - p1.x;
        dy = pt.y - p1.y;
      } else if (t > 1) {
        dx = pt.x - p2.x;
        dy = pt.y - p2.y;
      } else {
        dx = pt.x - p1.x - t * dx;
        dy = pt.y - p1.y - t * dy;
      }
      return Math.sqrt(dx * dx + dy * dy);
    }

  }

}