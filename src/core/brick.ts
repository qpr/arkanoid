module Game {

  export let brickColors: number[] = [ 0, 0xCDDC39, 0x8BC34A, 0x4CAF50 ];

  export class Brick extends PIXI.Graphics {

    public static width: number = 60;
    public static height: number = 30;

    constructor(type: number) {
      super();
      this.lineStyle(2, brickColors[type]);
      this.beginFill(brickColors[type], 0.5);
      this.drawRect(0, 0, Brick.width, Brick.height);
      this.endFill();
    }

    public getPoly() {
      return [
        new PIXI.Point(this.x,               this.y               ),
        new PIXI.Point(this.x + Brick.width, this.y               ),
        new PIXI.Point(this.x + Brick.width, this.y + Brick.height),
        new PIXI.Point(this.x,               this.y + Brick.height),
      ];
    }
  }

}