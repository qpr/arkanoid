/// <reference path='../utils/easing.ts'/>

module Game {

  export class Platform extends PIXI.Container {

    static step: number = 10;

    private platform: PIXI.Graphics;
    private speed: Easing;
    private size: number;
    private span: number;

    public onMove: (x: number) => void;

    constructor() {
      super();
      this.speed = new Easing(0.5, 0.5, 6.0);
      this.span = Position.w();
      this.size = 10;
      this.createFloor();
      this.createPlatform();
    }

    private createFloor() {
      let floor = new PIXI.Graphics();
      floor.beginFill(0xFFFFFF, 0.1);
      floor.drawRect(0, 22, this.span, 4);
      floor.endFill();
      this.addChild(floor);
    }

    private createPlatform() {
      this.platform = new PIXI.Graphics();
      this.platform.lineStyle(2, 0xE91E63, 1);
      this.platform.beginFill(0xE91E63, 0.5);
      this.platform.drawRoundedRect(0, 0, this.getSize(), 16, 5);
      this.platform.endFill();
      this.addChild(this.platform);
      this.reset();
    }

    public reset() {
      let pos = (this.span - this.getSize())/2;
      this.platform.x = pos;
      if (this.onMove != null) this.onMove(pos);
    }

    public update() {
      if (App.controls.isRight()) this.speed.increase();
      else if (App.controls.isLeft()) this.speed.decrease();
      else this.speed.fading();

      if (this.speed.value == 0) return;

      let pos = this.platform.x + this.speed.value;
      let right = this.span - this.getSize();
      if (pos < 0) {
        this.speed.reset();
        pos = 0;
      } else if (pos > right) {
        this.speed.reset();
        pos = right;
      }
      this.platform.x = pos;
      if (this.onMove != null) this.onMove(pos);
    }

    private getSize(): number {
      return this.size * Platform.step;
    }
  }
}