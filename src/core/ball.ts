module Game {

  export interface IBallState {
    speed: number;
    point: PIXI.Point;
    direction: Vector;
  }

  export class Ball extends PIXI.Graphics {

    public static radius: number = 10;

    private speed: number;
    private direction: Vector;

    constructor() {
      super();
      this.lineStyle(1, 0xFFD54F);
      this.beginFill(0xFFD54F, 0.5);
      this.drawCircle(0, 0, Ball.radius - 1);
      this.endFill();
      this.reset();
    }

    public reset() {
      this.speed = 5.0;
      this.position = Position.midpoint();
      let r = (Math.random()-0.5)/2;
      this.direction = new Vector(r, 1.0).normalize();
    }

    public update(moving: (state: IBallState) => IBallState) {
      let state: IBallState = moving({
        speed: this.speed,
        point: this.position,
        direction: this.direction
      });
      this.position = state.point;
      this.direction = state.direction;
    }
  }
}