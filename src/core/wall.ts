module Game {

  export class Wall extends PIXI.Container {

    private idx: number;
    private bricks: Brick[];

    constructor() {
      super();
      this.bricks = [];
      this.clear();
    }

    public build(schema: number[], cols: number) {
      this.clear();
      let offsetX = Position.center(cols * Brick.width);
      let offsetY = 40;
      for (let type of schema) {
        if (type > 0) {
          let brick = new Brick(type);
          brick.name = 'brick' + this.idx;
          brick.x = offsetX + (this.idx % cols) * Brick.width;
          brick.y = offsetY + (Math.floor(this.idx / cols)) * Brick.height;
          this.bricks.push(brick);
          this.addChild(brick);
        }
        this.idx++;
      }
    }

    public clear() {
      this.idx = 0;
      this.bricks = [];
      this.removeChildren();
    }

    public removeBrick(brick: Brick) {
      this.removeChild(brick);
      this.bricks = this.bricks.filter((br) => br != brick);
    }

    public hasBricks(): boolean {
      return this.bricks.length > 0;
    }

    public each(fn: (Brick) => void) {
      for (let brick of this.bricks) fn(brick);
    }
  }

}