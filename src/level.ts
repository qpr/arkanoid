/// <reference path='ui/score.ts'/>
/// <reference path='ui/screen.ts'/>
/// <reference path='core/ball.ts'/>
/// <reference path='core/wall.ts'/>
/// <reference path='core/brick.ts'/>
/// <reference path='core/platform.ts'/>
/// <reference path='utils/vector.ts'/>
/// <reference path='utils/collider.ts'/>
/// <reference path='utils/position.ts'/>

module Game {

  export class Level extends PIXI.Container {

    private ball: Ball;
    private wall: Wall;
    private score: Score;
    private platform: Platform;
    private collider: Collider;

    private startScreen: Screen;
    private pauseScreen: Screen;
    private loseScreen: Screen;
    private winScreen: Screen;

    public active: boolean;

    constructor() {
      super();

      this.platform = new Platform(Position.w());
      this.platform.position.set(Position.center(this.platform.width), Position.bottom(this.platform.height));
      this.addChild(this.platform);

      this.ball = new Ball();
      this.addChild(this.ball);

      // Boundary
      let topLeft = new PIXI.Point(0, 0);
      let topRight = new PIXI.Point(Position.w(), 0);
      let bottomLeft = new PIXI.Point(0, Position.h());
      let bottomRight = new PIXI.Point(Position.w(), Position.h());

      this.collider = new Collider();
      this.collider.addPoly([ bottomLeft, topLeft, topRight, bottomRight], 'boundary', false);
      this.collider.addSegment(bottomRight, bottomLeft, 'floor', () => {
        /* You lose when you touch the floor */
        this.active = false;
        App.state = State.Lose;
        this.loseScreen.show();
      });

      this.platform.onMove = (x: number) => {
        this.collider.movePlatform(x);
      };

      this.wall = new Wall();
      this.addChild(this.wall);

      this.startScreen = new Screen('ARKANOID', 'PRESS SPACE TO START');
      this.startScreen.show();
      this.addChild(this.startScreen);

      this.pauseScreen = new Screen('PAUSE', 'PRESS SPACE TO RESUME GAME');
      this.addChild(this.pauseScreen);

      this.winScreen = new Screen('YOU WIN!', 'PRESS SPACE TO START NEW GAME');
      this.addChild(this.winScreen);

      this.loseScreen = new Screen('YOU LOSE :(', 'PRESS SPACE TO START NEW GAME');
      this.addChild(this.loseScreen);

      this.score = new Score();
      this.score.position.set(Position.center(this.score.width), 10);
      this.addChild(this.score);

      this.active = false;
    }

    public pause() {
      this.active = false;
      this.pauseScreen.show();
    }

    public unpause() {
      this.active = true;
      this.pauseScreen.hide();
    }

    public reset() {
      this.score.set(0);
      this.ball.reset();
      this.platform.reset();
      this.winScreen.hide();
      this.loseScreen.hide();
      this.startScreen.hide();
      this.collider.removeBricks();
      this.buildWall();
    }

    private buildWall() {
      this.wall.build([
        0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
        0, 1, 1, 3, 3, 3, 3, 1, 1, 0,
        1, 2, 2, 2, 2, 2, 2, 2, 2, 1,
        0, 1, 1, 3, 3, 3, 3, 1, 1, 0,
        0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
      ], 10);
      this.wall.each((brick: Brick) => {
        this.collider.addPoly(brick.getPoly(), brick.name, true, () => {
          this.collider.removeSegments(brick.name);
          this.wall.removeBrick(brick);
          this.score.increase();
          if (!this.wall.hasBricks()) {
            this.active = false;
            App.state = State.Win;
            this.winScreen.show();
          }
        });
      });
    }

    public update() {
      if (!this.active) return;
      this.platform.update();
      this.ball.update(this.collider.move.bind(this.collider));
    }

  }

}