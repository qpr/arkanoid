/// <reference path='level.ts'/>
/// <reference path='utils/input.ts'/>

const enum State {
  Start,
  Game,
  Pause,
  Win,
  Lose
}

module App {

  export let width:    number = 768;
  export let height:   number = 576;
  export let controls: Input;
  export let ticker:   PIXI.ticker.Ticker;
  export let state:    State;

  export class Application {

    private renderer: PIXI.CanvasRenderer;
    private stage: PIXI.Container;
    private level: Game.Level;

    constructor() {

      this.renderer = new PIXI.CanvasRenderer(App.width, App.height, {
        backgroundColor: 0x212121
      });
      document.getElementById('app').appendChild(this.renderer.view);

      this.stage = new PIXI.Container();
      this.stage.interactive = true;

      App.state = State.Start;

      App.ticker = new PIXI.ticker.Ticker();
      App.ticker.autoStart = true;
      App.ticker.add(this.update);

      App.controls = new Input();
      App.controls.onAction(() => {
        switch (App.state) {
          case State.Start:
            App.state = State.Game;
            this.level.unpause();

          case State.Win:
          case State.Lose:
            App.state = State.Game;
            this.level.reset();
            this.level.unpause();
            break;

          case State.Pause:
            App.state = State.Game;
            this.level.unpause();
            break;

          case State.Game:
            App.state = State.Pause;
            this.level.pause();
            break;
        }
      });

      this.level = new Game.Level();
      this.stage.addChild(this.level);

    }

    private update = (): void => {
      this.level.update();
      this.renderer.render(this.stage);
    }

  }

}