module Game {

  export class Score extends PIXI.Container {

    private score: PIXI.Text;

    constructor(public value: number = 0) {
      super();

      this.alpha = 0.5;
      this.score = new PIXI.Text('SCORE: ' + this.value, {
        fill: '#FFFFFF',
        font: 'bold 14px Arial'
      });
      this.addChild(this.score);
    }

    public set(value: number) {
      this.value = value;
      this.score.text = 'SCORE: ' + this.value;
    }

    public increase() {
      this.set(this.value + 1);
    }

  }

}