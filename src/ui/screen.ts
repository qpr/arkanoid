module Game {

  export class Screen extends PIXI.Graphics {

    constructor(title: string, subtitle: string = '') {
      super();

      this.hide();
      this.beginFill(0x000000, 0.7);
      this.drawRect(0, 0, Position.w(), Position.h());
      this.endFill();

      let text = new PIXI.Text(title, {
        fill: '#FFFFFF',
        stroke: '#000000',
        strokeThickness : 4,
        font: 'bold 36px Arial'
      });
      text.position = Position.midpoint(text.width, text.height);
      this.addChild(text);

      let subtext = new PIXI.Text(subtitle, {
        fill: '#FFFFFF',
        font: 'bold 14px Arial'
      });
      subtext.alpha = 0.5;
      subtext.position = Position.midpoint(subtext.width, subtext.height - 70);
      this.addChild(subtext);
    }

    public show() {
      this.visible = true;
    }

    public hide() {
      this.visible = false;
    }
  }

}